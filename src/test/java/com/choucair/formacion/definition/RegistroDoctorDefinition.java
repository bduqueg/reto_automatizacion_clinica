package com.choucair.formacion.definition;

import com.choucair.formacion.steps.Reto1FormDoctorSteps;
import com.choucair.formacion.steps.Reto1ValidationSteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class RegistroDoctorDefinition {

    @Steps
    private Reto1ValidationSteps reto1ValidationSteps;
    @Steps
    private Reto1FormDoctorSteps reto1FormDoctorSteps;


    @Given("^que Carlos necesita registrar un nuevo doctor$")
    public void que_Carlos_necesita_registrar_un_nuevo_doctor(){
        reto1ValidationSteps.entrarPagina();

    }

    @When("^el realiza el registro del mismo en el aplicativo de Administracion de Hospitales$")
    public void el_realiza_el_registro_del_mismo_en_el_aplicativo_de_Administracion_de_Hospitales(){
        reto1FormDoctorSteps.llenarFormulario();

    }

    @Then("^el verifica que se presente en pantalla el mensaje Datos guardados correctamente$")
    public void el_verifica_que_se_presente_en_pantalla_el_mensaje_Datos_guardados_correctamente(){
        reto1FormDoctorSteps.verificarFormulario();

    }




}
