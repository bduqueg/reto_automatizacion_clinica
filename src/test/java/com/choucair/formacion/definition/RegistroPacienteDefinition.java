package com.choucair.formacion.definition;

import com.choucair.formacion.steps.Reto1FormDoctorSteps;
import com.choucair.formacion.steps.Reto1FormPacienteSteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class RegistroPacienteDefinition {

    @Steps
    private Reto1FormPacienteSteps reto1FormPacienteSteps;



    @Given("^que Carlos necesita registrar un nuevo paciente$")
    public void que_Carlos_necesita_registrar_un_nuevo_paciente(){
        reto1FormPacienteSteps.abrirPestanaPaciente();

    }

    @When("^se realiza el registro del paciente en el aplicativo de Administracion de Hospitales$")
    public void el_realiza_el_registro_del_paciente_en_el_aplicativo_de_Administracion_de_Hospitales(){
        reto1FormPacienteSteps.llenarFormularioPaciente();

    }

    @Then("^se verifica el registro del mismo en el aplicativo de Administracion de Hospitales$")
    public void el_realiza_el_registro_del_mismo_en_el_aplicativo_de_Administracion_de_Hospitales(){
        reto1FormPacienteSteps.verificarPacienteExito();


    }


}
