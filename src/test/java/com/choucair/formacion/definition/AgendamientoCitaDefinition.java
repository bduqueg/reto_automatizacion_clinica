package com.choucair.formacion.definition;

import com.choucair.formacion.steps.Reto1FormAgendarCitaSteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class AgendamientoCitaDefinition {

    @Steps
    private Reto1FormAgendarCitaSteps reto1FormAgendarCitaSteps;


    @Given("^que Carlos necesita asistir al medico$")
    public void que_Carlos_necesita_asistir_al_medico(){
        reto1FormAgendarCitaSteps.abrirPestanaCita();

    }

    @When("^el realiza el agendamiento de una Cita$")
    public void el_realiza_el_agendamiento_de_una_Cita(){
        reto1FormAgendarCitaSteps.agendarCita();

    }

    @Then("^verifica que se presente en pantalla el mensaje Datos guardados correctamente$")
    public void verifica_que_se_presente_en_pantalla_el_mensaje_Datos_guardados_correctamente(){
        reto1FormAgendarCitaSteps.verificarExito();

    }

}
