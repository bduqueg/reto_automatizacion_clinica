package com.choucair.formacion.pageobjects;


import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

import java.util.WeakHashMap;

@DefaultUrl("http://automatizacion.herokuapp.com/pperez/")
public class FormPacientePage extends PageObject {
    //Botón Pacientes
    @FindBy(xpath = "//*[@id=\'page-wrapper\']/div/div[2]/div/div/div/div/div[1]/div/a[2]")
    private WebElementFacade btnPacientes;

    //Etiqueta Pacientes
    @FindBy(xpath = "//*[@id=\'page-wrapper\']/div/div[1]/div/h1/small")
    private WebElementFacade lblPacientes;

    //Campo Nombre
    @FindBy(xpath = "//*[@id=\'page-wrapper\']/div/div[3]/div/div[1]/input")
    private WebElementFacade txtNombre;

    //Campo Apellidos
    @FindBy(xpath ="//*[@id=\"page-wrapper\"]/div/div[3]/div/div[2]/input")
    private WebElementFacade txtApellido;

    //Campo Telefono
    @FindBy(xpath = "//*[@id=\'page-wrapper\']/div/div[3]/div/div[3]/input")
    private WebElementFacade txtTelefono;

    //Campo tipo de documento
    @FindBy(xpath = "//*[@id=\'page-wrapper\']/div/div[3]/div/div[4]/select")
    private WebElementFacade cmbDocumento;

    //Campo Documento
    @FindBy(xpath = "//*[@id=\'page-wrapper\']/div/div[3]/div/div[5]/input")
    private WebElementFacade txtDocumento;

    //Check Salud prepagada
    @FindBy(xpath = "//*[@id=\"page-wrapper\"]/div/div[3]/div/div[6]/label/input")
    private WebElementFacade checkSalud;

    //Botón
    @FindBy(xpath = "//*[@id=\'page-wrapper\']/div/div[3]/div/a")
    private WebElementFacade btnEnviar;

    //Texto verficación
    @FindBy(xpath = "//*[@id=\"page-wrapper\"]/div/div[2]/div[1]/h3")
    private WebElementFacade  lblVerficarPaciente;

    public void ingresarPaciente(){

        btnPacientes.click();
    }
    public void verificaPestanaPaciente(){
        String strMensaje = lblPacientes.getText();
        assertThat(strMensaje,containsString("Agregar paciente"));
    }
    public void nombrePaciente(){
        txtNombre.click();
        txtNombre.sendKeys("Paciente");
    }
    public void apellidoPaciente(){
        txtApellido.click();
        txtApellido.sendKeys("Apellido Paciente");
    }
    public void telefonoPaciente(){
        txtTelefono.click();
        txtTelefono.sendKeys("66666666");
    }
    public void selectDocumento(){
        cmbDocumento.click();
        cmbDocumento.selectByVisibleText("Cédula de ciudadanía");
    }
    public void documentoPaciente(){
        txtDocumento.click();
        txtDocumento.sendKeys("1212");


    }
    public void checkSaludPaciente(){
        checkSalud.click();
    }
    public void enviarPaciente(){
        btnEnviar.click();
    }
    public void verificarPaciente(){
        String txtMensaje = lblVerficarPaciente.getText();
        assertThat(txtMensaje,containsString("Guardado:"));
    }


}
