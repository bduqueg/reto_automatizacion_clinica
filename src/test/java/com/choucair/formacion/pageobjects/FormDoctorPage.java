package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class FormDoctorPage extends PageObject {
    //Campo Nombre
    @FindBy(xpath = "//*[@id=\'name\']")
    private WebElementFacade txtNombre;

    //Campo Apellidos
    @FindBy(xpath = "//*[@id=\'last_name\']")
    private WebElementFacade txtApellidos;

    //Campo Telefono
    @FindBy(xpath = "//*[@id=\'telephone\']")
    private WebElementFacade txtTelefono;

    //Campo tipo de documento
    @FindBy(xpath = "//*[@id=\'identification_type\']")
    private WebElementFacade cmbDocumento;

    //Campo Documento
    @FindBy(xpath = "//*[@id=\'identification\']")
    private WebElementFacade txtCedula;

    //Botón guardar
    @FindBy(xpath = "//*[@id=\'page-wrapper\']/div/div[3]/div/a")
    private WebElementFacade btnGuardar;

    @FindBy(xpath = "//*[@id=\'page-wrapper\']/div/div[2]/div[1]/h3")
    private WebElementFacade lblGuardado;

    public void nombreDoctor(){
        txtNombre.click();
        txtNombre.sendKeys("Doctor");
        //txtNombre.type("miguel");

    }
    public void apellidosDoctor(){
        txtApellidos.click();
        txtApellidos.sendKeys("Apellido Doctor1");
    }
    public void telefonoDoctor(){
        txtTelefono.click();
        txtTelefono.sendKeys("555555555");
    }

    public void multipleSelectDocumento(){
        cmbDocumento.click();
        cmbDocumento.selectByVisibleText("Cédula de ciudadanía");
    }
    public void documentoDoctor(){
        txtCedula.click();
        txtCedula.sendKeys("1313");
    }
    public void guardarDoctor(){
        btnGuardar.click();
    }

    public void verficarFormularioCompleto(){
        String strMensaje = lblGuardado.getText();
        assertThat(strMensaje,containsString("Guardado:"));
    }

}
