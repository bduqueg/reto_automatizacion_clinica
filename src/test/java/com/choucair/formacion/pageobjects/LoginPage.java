package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

@DefaultUrl("http://automatizacion.herokuapp.com/pperez/")

public class LoginPage extends PageObject {
    @FindBy(xpath = "//*[@id=\"page-wrapper\"]/div/div[2]/div/div/div/div/div[1]/div/a[1]")
    private WebElementFacade btnAgregarDoctor;

    @FindBy(xpath = "//*[@id=\'page-wrapper']/div/div[1]/div/h1/small")
    private WebElementFacade lblValidationDoctor;

    public void ingresarDoctor(){
        btnAgregarDoctor.click();

    }
    public void verificaPestanaDoctor(){
        String strMensaje = lblValidationDoctor.getText();
        assertThat(strMensaje,containsString("Agregar doctor"));
    }


}
