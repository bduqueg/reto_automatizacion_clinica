package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

import com.choucair.formacion.pageobjects.FormPacientePage;

import org.openqa.selenium.Keys;


@DefaultUrl("http://automatizacion.herokuapp.com/pperez/")
public class FormAsignarCitaPage extends PageObject {
    private FormPacientePage formPacientePage;

    //Botón asignar cita
    @FindBy(xpath = "//*[@id=\'page-wrapper\']/div/div[2]/div/div/div/div/div[1]/div/a[6]")
    private WebElementFacade btnAsignarCita;

    //etiqueta asignar cita
    @FindBy(xpath = "//*[@id=\'page-wrapper\']/div/div[1]/div/h1/small")
    private WebElementFacade lblAsignarCita;

    //Mes/dia/año -campo Dia de la cita
    @FindBy(xpath = "//*[@id=\'datepicker\']")
    private WebElementFacade dateCita;

    //Documento de identidad paciente
    @FindBy(xpath = "//*[@id=\'page-wrapper\']/div/div[3]/div/div[2]/input")
    private WebElementFacade txtdocumentoPaciente;

    //Documento de identidad doctor
    @FindBy(xpath = "//*[@id=\"page-wrapper\"]/div/div[3]/div/div[3]/input")
    private WebElementFacade txtdocumentoDoctor;

    //Observaciones
    @FindBy(xpath = "//*[@id=\'page-wrapper\']/div/div[3]/div/div[4]/textarea")
    private WebElementFacade txtObservaciones;

    //botón observaciones
    @FindBy(xpath = "//*[@id=\'page-wrapper\']/div/div[3]/div/a")
    private WebElementFacade btnEnviar;

    //etiqueta de exito cita (Guardado:)
    @FindBy(xpath = "//*[@id=\'page-wrapper\']/div/div[2]/div[1]/h3")
    private WebElementFacade lblExitoCita;

    public void botonAsignarCita(){
        btnAsignarCita.click();
    }
    /*public void verificarPestanaCitas(){
        String txtMensaje = lblAsignarCita.getText();
        assertThat(txtMensaje,containsString(" Agendar cita"));
    }*/
    public void diaCita(){

        dateCita.sendKeys("08/31/2019");

    }
    public void documentoPaciente(){

        txtdocumentoPaciente.sendKeys("1212");

    }
    public void documentoDoctor(){

        txtdocumentoDoctor.sendKeys("1313");
    }
    public void observaciones(){
        txtObservaciones.sendKeys("Observaciones de la cita");
    }
    public void enviarCita(){
        btnEnviar.click();
    }
    public void verificarExitoCita(){
        String txtMensaje = lblExitoCita.getText();
        assertThat(txtMensaje,containsString("Guardado:"));
    }

}
