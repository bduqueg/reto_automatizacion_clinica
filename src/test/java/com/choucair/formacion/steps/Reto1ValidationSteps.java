package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.FormDoctorPage;
import com.choucair.formacion.pageobjects.LoginPage;
import net.thucydides.core.annotations.Step;

public class Reto1ValidationSteps {

    private LoginPage loginPage;

    @Step
    public void entrarPagina() {
       //abrir la pagina
        loginPage.open();
        //Ingresa a la pestaña doctor
        loginPage.ingresarDoctor();
        //Verifica que este en la pestaña de Doctor
        loginPage.verificaPestanaDoctor();

    }

}
