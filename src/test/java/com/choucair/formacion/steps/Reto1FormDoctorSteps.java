package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.FormDoctorPage;
import net.thucydides.core.annotations.Step;

public class Reto1FormDoctorSteps {

    private FormDoctorPage formDoctorPage;

    @Step
    public void llenarFormulario() {
        //método de llenar el nombre del doctor
        formDoctorPage.nombreDoctor();
        //método de llenar el apellido del doctor
        formDoctorPage.apellidosDoctor();
        //método de llenar el telefono del doctor
        formDoctorPage.telefonoDoctor();
        //método de escoger el tipo de documento
        formDoctorPage.multipleSelectDocumento();
        //método de digitar el documento del doctor
        formDoctorPage.documentoDoctor();
        //método de darle click al botón guardar
        formDoctorPage.guardarDoctor();
    }
    @Step
    public void verificarFormulario(){
        //método que verifica que guardó
        formDoctorPage.verficarFormularioCompleto();
    }
}
