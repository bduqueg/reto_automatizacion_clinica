package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.FormPacientePage;
import net.thucydides.core.annotations.Step;

public class Reto1FormPacienteSteps {
    private FormPacientePage formPacientePage;

    @Step
    public void abrirPestanaPaciente() {
        formPacientePage.open();
        formPacientePage.ingresarPaciente();
        formPacientePage.verificaPestanaPaciente();

    }

    @Step
    public void llenarFormularioPaciente() {
        formPacientePage.nombrePaciente();
        formPacientePage.apellidoPaciente();
        formPacientePage.telefonoPaciente();
        formPacientePage.selectDocumento();
        formPacientePage.documentoPaciente();
        formPacientePage.checkSaludPaciente();
        formPacientePage.enviarPaciente();
    }

    @Step
    public void verificarPacienteExito(){
        formPacientePage.verificarPaciente();
    }
}
