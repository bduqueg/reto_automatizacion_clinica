package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.FormAsignarCitaPage;
import net.thucydides.core.annotations.Step;

public class Reto1FormAgendarCitaSteps {

    private FormAsignarCitaPage formAsignarCitaPage;

    @Step
    public void abrirPestanaCita() {
        formAsignarCitaPage.open();
        formAsignarCitaPage.botonAsignarCita();
        //formAsignarCitaPage.verificarPestanaCitas();
    }
    @Step
    public void agendarCita() {
        formAsignarCitaPage.diaCita();
        formAsignarCitaPage.documentoPaciente();
        formAsignarCitaPage.documentoDoctor();
        formAsignarCitaPage.observaciones();
        formAsignarCitaPage.enviarCita();
    }
    @Step
    public void verificarExito(){
        formAsignarCitaPage.verificarExitoCita();
    }


}
