

@Regresion
    Feature: Gestionar cita médica
      Como paciente
      Quiero realizar la solicitud de una cita médica
      A través del sistema de Administración de Hospitales

  @RegistroDoctor
    Scenario: Realizar el Registro de un Doctor
    Given que Carlos necesita registrar un nuevo doctor
    When el realiza el registro del mismo en el aplicativo de Administracion de Hospitales
    Then el verifica que se presente en pantalla el mensaje Datos guardados correctamente

  @RegistroPaciente
    Scenario: Realizar el Registro de un Paciente
    Given que Carlos necesita registrar un nuevo paciente
    When se realiza el registro del paciente en el aplicativo de Administracion de Hospitales
    Then se verifica el registro del mismo en el aplicativo de Administracion de Hospitales

  @AgendarCita
    Scenario: Realizar el Agendamiento de una Cita
    Given que Carlos necesita asistir al medico
    When el realiza el agendamiento de una Cita
    Then verifica que se presente en pantalla el mensaje Datos guardados correctamente












